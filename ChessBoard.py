class ChessState():
    """
    A class that represents state of chess game
        1. Representing current state of the pieces
        2. Keeping log of moves
        3. Generating valid moves
        4. Keeping track of pins and checks
    """
    def __init__(self):
        """
        Parameters
        ----------------------
        Whiteturn : bool
            Is white to move
        log : list
            Register of all moves made 
        white/black_king_pos : tuple
            Position of white or black king
        pins : list
            List of all pins in current position
        checks : list
            List of checks in current position
        inCheck : bool
            If king is in check in current position
        """
        self.board = [
            ["cW", "cS", "cG", "cH", "cK", "cG", "cS", "cW"],
            ["cp", "cp", "cp", "cp", "cp", "cp", "cp", "cp"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["--", "--", "--", "--", "--", "--", "--", "--"],
            ["bp", "bp", "bp", "bp", "bp", "bp", "bp", "bp"],
            ["bW", "bS", "bG", "bH", "bK", "bG", "bS", "bW"],
        ]
        self.Whiteturn = True
        self.log = []
        self.white_king_pos = (7,4)
        self.black_king_pos = (0,4)
        self.pins = []
        self.checks = []
        self.inCheck = False

    def make_move(self, move):
        """
        Keeps track of any king move, updates board after a move, appends last move to log, change the turn
        ----------------------------
        Parameters:
            move : instance of move class

        """
        self.board[move.start_square[0]][move.start_square[1]] = "--"
        self.board[move.end_square[0]][move.end_square[1]] = move.piece_moved
        if move.piece_moved == "cK":
            self.black_king_pos = move.end_square
        if move.piece_moved == "bK":
            self.white_king_pos = move.end_square
        self.log.append(move)
        self.check_if_move_promotes()
        self.Whiteturn = not self.Whiteturn

    def get_all_possible_moves(self):
        """
        Generates all possible moves for pieces of color to move
        """
        self.pins, self.checks, self.inCheck = self.check_if_pinned_of_checked() #Update the state of parameters
        moves = []
        for row in range(len(self.board)):
            for column in range(len(self.board[row])):
                turn = self.board[row][column][0] #Check the color of a piece on given (row,column)
                if (turn == "b" and self.Whiteturn) or (turn == "c" and not self.Whiteturn): #Check only pieces of color to move
                    piece = self.board[row][column][1]  
                    if piece == "K":
                        self.get_king_valid_moves(row, column, moves)
                    if piece == "p":
                        self.get_pawn_valid_moves(row, column, moves)
                    if piece == "W":
                        self.get_rook_valid_moves(row, column, moves)
                    if piece == "H":
                        self.get_queen_valid_moves(row,column,moves)
                    if piece == "S":
                        self.get_knight_valid_moves(row, column, moves)
                    if piece == "G":
                        self.get_bishop_valid_moves(row, column, moves)
        self.check_if_castle_possible(moves)

        return moves

    def get_all_valid_moves(self):
        """ 
        Get all valid moves in a position
        """
        moves = self.get_all_possible_moves()  
        valid_squares = set()  #Valid end_squares for pieces to move if the king is checked
        if len(self.checks) == 1:
            if "S" in self.checks[0]:   #If knight give a check you either need to move or take it
                valid_squares.add(self.checks[0][0])
            else:
                #Checking all squares in the direction of the check
                for i in range(1,8):
                    if self.Whiteturn:
                        x = self.white_king_pos[0] + self.checks[0][1][0] * i
                        y = self.white_king_pos[1] + self.checks[0][1][1] * i

                        if 0 <= x < 8 and 0 <= y < 8:
                            checked_piece = self.board[x][y][0]
                            if checked_piece == "c":    #End when white hits black piece
                                valid_squares.add((x,y))
                                break 
                            else:
                                valid_squares.add((x,y))
                    else:
                        x = self.black_king_pos[0] + self.checks[0][1][0] * i
                        y = self.black_king_pos[1] + self.checks[0][1][1] * i
                        
                        if 0 <= x < 8 and 0 <= y < 8:
                            checked_piece = self.board[x][y][0]
                        
                            if checked_piece == "b":    #End when black hits a white piece
                                valid_squares.add((x,y))
                                break 
                            else:
                                valid_squares.add((x,y))
                                
            for i in range(len(moves) - 1, -1, -1):  #Checking list from last element
                if (moves[i].end_square[0], moves[i].end_square[1]) not in valid_squares: 
                    if "K" not in moves[i].piece_moved:  #If the end sqaure is not in valid_squares AND if it isn't a king move it gets removed
                        moves.remove(moves[i])
        # You need to move if it is a double check so only get the king moves
        elif len(self.checks) == 2:
            moves = []
            if self.Whiteturn:
                self.get_king_valid_moves(self.white_king_pos[0], self.white_king_pos[1], moves)
            else:
                self.get_king_valid_moves(self.black_king_pos[0], self.black_king_pos[1], moves)
        return moves

    def check_if_pinned_of_checked(self):
        """
        Check if piece is pinned to the king or if there are any checks on the board
        
        returns pins, checks, inCheck
        """
        pins = []
        checks = []
        inCheck = False

        if self.Whiteturn:
            ally_color = "b"
            enemy_color = "c"
            start_row = self.white_king_pos[0]
            start_col = self.white_king_pos[1]
            pawn_check = [(-1,-1), (-1,1)]

        if not self.Whiteturn:
            ally_color = "c"
            enemy_color = "b"
            start_row = self.black_king_pos[0]
            start_col = self.black_king_pos[1]
            pawn_check = [(1,-1), (1,1)]

        knight_moves = [(2,1), (1,2), (-1,2), (-2,1), (-2,-1), (-1, -2), (1,-2), (2,-1)]
        directions = [(-1, -1), (1,1), (-1,1), (1,-1), (-1, 0), (1,0), (0,1), (0,-1)]

        for i in range(len(directions)):
            possible_pins = ()  #Resets possible pins in a direction
            d = directions[i]
            for m in range(1,8):
                x = start_row + d[0] * m  
                y = start_col + d[1] * m
                if 0 <= x < 8 and 0 <= y < 8:
                    piece_checked = self.board[x][y][0]
                    if abs(d[0]) == 1 and abs(d[1]) == 1:  #can be pinned/checked diagonally
                        if piece_checked == ally_color:    #If the first checked is allied piece there is a possiblty of pin
                            if possible_pins == ():
                                possible_pins = ((x,y), d, self.board[x][y][1])
                            else:
                                break  #if there are two allied pieces possible_pins gets reset in line 165 after break
                        elif piece_checked == enemy_color: 
                            piece_type = self.board[x][y][1]
                            if (piece_type == "G" or piece_type == "H") and possible_pins == ():  #If first checked piece is enemy bishiop/queen append check
                                inCheck = True
                                checks.append([(x, y), (d[0], d[1]), piece_type])
                                break
                            elif (piece_type == "p" or piece_type == "S" or piece_type == "W" or piece_type == "K"): #If first checked is not enemy bishop/queen break
                                break
                            elif (piece_type == "G" or piece_type == "H") and possible_pins: 
                                pins.append(possible_pins)
                                break

                    elif abs(d[0]+d[1]) == 1:     #can be pinned/checked horizontally works the similarly to the diagonal moves
                        if piece_checked == ally_color:
                            if possible_pins == ():  
                                possible_pins = ((x,y), d, self.board[x][y][1])
                            else:
                                break   
                        elif piece_checked == enemy_color:
                            piece_type = self.board[x][y][1]
                            if (piece_type == "H" or piece_type == "W") and possible_pins == ():
                                inCheck = True
                                checks.append([(x, y), (d[0], d[1]), piece_type])
                                break
                            elif (piece_type == "H" or piece_type == "W") and len(possible_pins):
                                pins.append(possible_pins)
                                break
                            elif (piece_type == "p" or piece_type == "S" or piece_type == "G" or piece_type == "K"):
                                break
        
        # Checks for knight moves as knights cannot pin
        for d in knight_moves:
            x = start_row + d[0]
            y = start_col + d[1]
            if 0 <= x < 8 and 0 <= y < 8:
                color_of_piece = self.board[x][y][0]
                piece_type = self.board[x][y][1]
                if color_of_piece == enemy_color and piece_type == "S":
                    inCheck = True
                    checks.append([(x, y), (d[0], d[1]), piece_type])
                    break

        # Checks for pawn moves as pawns cannot pin
        for d in pawn_check:
            x = start_row + d[0]
            y = start_col + d[1]
            if 0 <= x < 8 and 0 <= y < 8:
                color_of_piece = self.board[x][y][0]
                piece_type = self.board[x][y][1]
                if color_of_piece == enemy_color and piece_type == "p":
                    inCheck = True
                    checks.append([(x, y), (d[0], d[1]), piece_type])
                    break

        return pins, checks, inCheck


    def check_if_move_was_pass(self):
        """
        If the move was enpassant capture the pawn behind
        """
        if self.log[-1].piece_captured == "--" and "p" in self.log[-1].piece_moved:
            if self.log[-1].start_square[1] != self.log[-1].end_square[1]: # If the pawn captured -- and moved diagonally the move was enpassant
                if self.Whiteturn:
                    self.board[self.log[-1].end_square[0]-1][self.log[-1].end_square[1]] = "--"
                else:
                    self.board[self.log[-1].end_square[0]+1][self.log[-1].end_square[1]] = "--"

    def check_if_move_was_castle(self):
        """
        If the move was castles move the rook from starting square to proper position after castles
        """
        if "K" in self.log[-1].piece_moved:
            if self.log[-1].start_square[1]-self.log[-1].end_square[1] < -1.5:
                if self.Whiteturn:
                    x = self.black_king_pos[0]
                    y = self.black_king_pos[1]
                    self.board[x][y+1] = "--"
                    self.board[x][y-1] = "cW"
                else:
                    x = self.white_king_pos[0]
                    y = self.white_king_pos[1]
                    self.board[x][y+1] = "--"
                    self.board[x][y-1] = "bW"
            elif self.log[-1].start_square[1]-self.log[-1].end_square[1] > 1.5:
                if self.Whiteturn:
                    x = self.black_king_pos[0]
                    y = self.black_king_pos[1]
                    self.board[x][y-2] = "--"
                    self.board[x][y+1] = "cW"
                else:
                    x = self.white_king_pos[0]
                    y = self.white_king_pos[1]
                    self.board[x][y-2] = "--"
                    self.board[x][y+1] = "bW"



    def check_if_move_promotes(self):
        """
        Promotes to the queen if pawn reaches the end of the board
        """
        if self.log[-1].piece_moved == 'bp' and self.log[-1].end_square[0] == 0:
            self.board[self.log[-1].end_square[0]][self.log[-1].end_square[1]] = "bH"
        elif self.log[-1].piece_moved == 'cp' and self.log[-1].end_square[0] == 7:
            self.board[self.log[-1].end_square[0]][self.log[-1].end_square[1]] = "cH"

    def get_pawn_valid_moves(self, row, column, moves):
        """Get all valid moves for pawn at location row,column"""
        pinned = self.check_if_piece_is_pinned(row,column)
        pin = ""
        if pinned:
            if abs(pinned[1][0])+abs(pinned[1][1]) == 2:
                pin = "d"
            else:
                pin = "h" 

        if self.Whiteturn:
            if pin != "d":
                if self.board[row-1][column] == "--":
                    moves.append(move((row,column),(row-1,column),self.board))
                    if row == 6 and self.board[row-2][column] == "--":
                        moves.append(move((row,column),(row-2,column),self.board))
            try:
                if not pinned:
                    if (self.board[row-1][column-1] != "--") and (self.board[row-1][column-1][0] == "c"):
                        moves.append(move((row,column),(row-1,column-1),self.board))
                    if (self.board[row-1][column+1] != "--") and (self.board[row-1][column+1][0] == "c"):
                        moves.append(move((row,column),(row-1,column+1),self.board))
                    self.check_if_enpassant_possible(row, column, moves)
                elif pin == "d":
                    if (self.board[row+pinned[1][0]][column+pinned[1][1]][0] == "c"):
                        moves.append(move((row,column),(row+pinned[1][0],column+pinned[1][1]),self.board))
            except:
                pass

        if not self.Whiteturn:
            if pin != "d":
                if self.board[row+1][column] == "--":
                    moves.append(move((row,column),(row+1,column),self.board))
                    if row == 1 and self.board[row-2][column]:
                        moves.append(move((row,column),(row+2,column),self.board))
            try:
                if not pinned:
                    if (self.board[row+1][column-1] != "--") and (self.board[row+1][column-1][0] == "b"):
                        moves.append(move((row,column),(row+1,column-1),self.board))
                    if (self.board[row+1][column+1] != "--") and (self.board[row+1][column+1][0] == "b"):
                        moves.append(move((row,column),(row+1,column+1),self.board))
                    self.check_if_enpassant_possible(row, column, moves)
                elif pin == "d":
                    if (self.board[row+pinned[1][0]][column+pinned[1][1]][0] == "b"):
                        moves.append(move((row,column),(row+pinned[1][0],column+pinned[1][1]),self.board))                    
            except:
                pass

    def check_if_piece_is_pinned(self, row, column):
        """
        Check if piece at given (row,column) is pinned to the king
        """
        for pin in self.pins:
            if (row, column) == pin[0]:
                return pin
            else:
                return False
        
    def get_queen_valid_moves(self, row, column, moves):
        self.get_horizontal_moves(row,column,moves)
        self.get_diagonal_moves(row,column,moves)

    def get_rook_valid_moves(self, row, column, moves):
        self.get_horizontal_moves(row, column, moves)

    def get_knight_valid_moves(self, row, column, moves):
        knight_moves = [(2,1), (1,2), (-1,2), (-2,1), (-2,-1), (-1, -2), (1,-2), (2, -1)]
        pinned = self.check_if_piece_is_pinned(row,column)
        print(pinned)
        if self.Whiteturn:
            if not pinned:
                for km in knight_moves:
                    x = row + km[0]
                    y = column + km[1]
                    if 0 <= x < 8 and 0 <= y < 8:
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x,y), self.board))
                        if (self.board[x][y] != "--") and (self.board[x][y][0] == "c"):
                            moves.append(move((row,column),(x, y), self.board))
                        if (self.board[x][y] != "--") and (self.board[x][y][0] == "c"):
                            moves.append(move((row,column),(x,y),self.board))

        if not self.Whiteturn:
            if not pinned:
                for km in knight_moves:
                    x = row + km[0]
                    y = column + km[1]
                    if 0 <= x < 8 and 0 <= y < 8:
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x,y), self.board))
                        if (self.board[x][y] != "--") and (self.board[x][y][0] == "b"):
                            moves.append(move((row,column),(x, y), self.board))
                        if (self.board[x][y] != "--") and (self.board[x][y][0] == "b"):
                            moves.append(move((row,column),(x,y),self.board))

            

    def get_bishop_valid_moves(self, row, column, moves):
        self.get_diagonal_moves(row,column,moves)

    def get_king_valid_moves(self, row, column, moves):
        directions = [(-1, -1), (1,1), (-1,1), (1,-1), (-1, 0), (1,0), (0,1), (0,-1)]
        if self.Whiteturn:
            king_pos = self.white_king_pos

            for d in directions:
                x = row + d[0]
                y = column + d[1]
                if 0 <= x < 8 and 0 <= y < 8:
                    self.white_king_pos = (x,y)
                    inCheck = self.check_if_pinned_of_checked()[2]  
                    if not inCheck: #Check if the king in the new position is checked if no then append if yes then don't
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x, y),self.board))
                        elif (self.board[x][y] != "--") and (self.board[x][y][0] == "c"):
                            moves.append(move((row,column), (x, y), self.board))
            self.white_king_pos = king_pos
        
        if not self.Whiteturn:
            king_pos = self.black_king_pos

            for d in directions:
                x = row + d[0]
                y = column + d[1]
                if 0 <= x < 8 and 0 <= y < 8:
                    self.black_king_pos = (x,y)
                    inCheck = self.check_if_pinned_of_checked()[2]
                    if not inCheck:
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x, y),self.board))
                        elif (self.board[x][y] != "--") and (self.board[x][y][0] == "b"):
                            moves.append(move((row,column), (x, y), self.board))
            self.black_king_pos = king_pos


    def get_diagonal_moves(self,row,column,moves):
        pinned = self.check_if_piece_is_pinned(row,column)
        if not pinned:
            directions = [(-1, -1), (1,1), (-1,1), (1,-1)]
        else:
            directions = [pinned[1], (-pinned[1][0],-pinned[1][1])] #If piece is pinned it can only move in the direction of the pin and backwards

        if self.Whiteturn:
            for d in directions:
                for i in range(1,7):
                    x = row + d[0]*i
                    y = column + d[1]*i
                    if 0 <= x < 8 and 0 <= y <8:
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x, y),self.board))
                        elif (self.board[x][y] != "--") and (self.board[x][y][0] == "c"):
                            moves.append(move((row,column), (x, y), self.board))
                            break
                        else:
                            break

        if not self.Whiteturn:
            for d in directions:
                for i in range(1,7):
                    x = row + d[0]*i
                    y = column + d[1]*i
                    if 0 <= x < 8 and 0 <= y <8:
                        if self.board[x][y] == "--":
                            moves.append(move((row,column),(x, y),self.board))
                        elif (self.board[x][y] != "--") and (self.board[x][y][0] == "b"):
                            moves.append(move((row,column), (x, y), self.board))
                            break
                        else:
                            break

    def get_horizontal_moves(self,row,column,moves):   
        pinned = self.check_if_piece_is_pinned(row,column)
        if not pinned:
            directions = [(-1, 0), (1,0), (0,1), (0,-1)]
        else:
            directions = [pinned[1], (-pinned[1][0],-pinned[1][1])]  #If piece is pinned it can only move in the direction of the pin and backwards

        if self.Whiteturn:
            for d in directions:
                for i in range(1,7):
                    x = row + d[0]*i
                    y = column + d[1]*i
                    if x < 0 or y < 0 or x > 7 or y > 7:
                        break
                    if self.board[x][y] == "--":
                        moves.append(move((row,column),(x, y),self.board))
                    elif (self.board[x][y] != "--") and (self.board[x][y][0] == "c"):
                        moves.append(move((row,column), (x, y), self.board))
                        break
                    else:
                        break

        if not self.Whiteturn:
            for d in directions:
                for i in range(1,7):
                    x = row + d[0]*i
                    y = column + d[1]*i
                    if x < 0 or y < 0 or x > 7 or y > 7:
                        break
                    if self.board[x][y] == "--":
                        moves.append(move((row,column),(x, y),self.board))
                    elif (self.board[x][y] != "--") and (self.board[x][y][0] == "b"):
                        moves.append(move((row,column), (x, y), self.board))
                        break
                    else:
                        break
        
    def checkmate_or_stalemate(self, moves):
        if self.inCheck and len(moves) == 0:
            return True
        elif not self.inCheck and (len(moves)) == 0:
            return False

    def check_if_castle_possible(self, moves):
        """
            There are 3 conditions to castlemove to be possible:
            1. King or castling rook couldn't move before
            2. Last rank from king to rook to be clear
            3. King cannot move through check

            If castles is possible the move is appended
        """

        if self.Whiteturn:
            short_castle = True
            short_side = [(7,6), (7,5)]
            long_castle = True
            long_side = [(7,1), (7,2), (7,3)]

            # Checking 1st condition"
            for d in self.log:
                if d.piece_moved == "bK":
                    return False
                if d.piece_moved == "bW" and d.start_square == (7,7):
                    short_castle = False
                if d.piece_moved == "bW" and d.start_square == (7,0):
                    long_castle = False

            #Checking 2nd condition"
            for x,y in short_side:
                if self.board[x][y] != "--":
                    short_castle = False
                    break

            for x,y in long_side:
                if self.board[x][y] != "--":
                    long_castle = False
                    break

            #Checking 3rd condition"
            king_pos = self.white_king_pos
            for x in short_side:
                self.white_king_pos = x
                inCheck = self.check_if_pinned_of_checked()[2]
                if inCheck:
                    short_castle = False
                    break

            for x in long_side:
                self.white_king_pos = x
                inCheck = self.check_if_pinned_of_checked()[2]
                if inCheck:
                    long_castle = False
                    break

            self.white_king_pos = king_pos
            if short_castle:
                moves.append(move((7,4), (7,6), self.board))
            elif long_castle:
                moves.append(move((7,4), (7,2), self.board))

        

        if not self.Whiteturn:
            short_castle = True
            short_side = [(0,6), (0,5)]
            long_castle = True
            long_side = [(0,1), (0,2), (0,3)]
            #Checking 1st condition"
            for d in self.log:
                if d.piece_moved == "cK":
                    return False
                if d.piece_moved == "cW" and d.start_square == (0,7):
                    short_castle = False
                if d.piece_moved == "cW" and d.start_square == (0,0):
                    long_castle = False

            if not short_castle and not long_castle: return False

            #Checking 2nd condition"
            for x,y in short_side:
                if self.board[x][y] != "--":
                    short_castle = False
                    break

            for x,y in long_side:
                if self.board[x][y] != "--":
                    long_castle = False
                    break

            if not short_castle and not long_castle: return False

            #Checking 3nd condition"
            king_pos = self.black_king_pos
            for x in short_side:
                self.black_king_pos = x
                inCheck = self.check_if_pinned_of_checked()[2]
                if inCheck:
                    short_side = False
                    break

            for x in long_side:
                self.black_king_pos = x
                inCheck = self.check_if_pinned_of_checked()[2]
                if inCheck:
                    long_side = False
                    break

            self.black_king_pos = king_pos
            if short_castle:
                moves.append(move((0,4), (0,6), self.board))
            elif long_castle:
                moves.append(move((0,4), (0,2), self.board))


    def check_if_enpassant_possible(self, row, column, moves):
        """
        Append enpassent to the move list if it is possible
        Add pinned enpassant #TODO
        """
        if abs(self.log[-1].start_square[0] - self.log[-1].end_square[0]) == 2:
            if (row,column-1) == self.log[-1].end_square:
                if self.Whiteturn:
                    #to the left
                    moves.append(move((row,column),(row-1,column-1), self.board, enpessant=True))
                else:
                    #to the right
                    moves.append(move((row,column),(row+1,column-1), self.board, enpessant=True))
            if (row,column+1) == self.log[-1].end_square:
                if self.Whiteturn:
                    moves.append(move((row,column),(row-1,column+1), self.board, enpessant=True))
                else:
                    moves.append(move((row,column),(row+1,column+1), self.board, enpessant=True))
        else:
            return False

class move():
    """
    A class that is representation of a chess move
    This class:
        1. Represent a chess move
        2. Can be use to give moves a proper chess notation     
    """
    dict_row = {1: "7", 2: "6", 3: "5", 4: "4", 5: "3", 6: "2", 7: "1", 0: "8"}
    dict_col = {0: "a", 1: "b", 2: "c", 3: "d", 4: "e", 5: "f", 6: "g", 7: "h"}

    def __init__(self, start_square, end_square, board, enpessant=None):
        """
        Parameters
        ----------------------
        start_square : tuple
            The starting square of the move
        end_square : tuple
            The end square of the move 
        piece_moved : str
            Piece moved ex. bS, cp    
        piece_captured : str
            Piece that was caputred on the end_square
        move_id : int
            Unique ID for each move
        """ 
        self.start_square = start_square
        self.end_square = end_square
        self.piece_moved = board[self.start_square[0]][self.start_square[1]]
        self.piece_captured = board[self.end_square[0]][self.end_square[1]]
        self.move_id = 1000*start_square[0] + 100*start_square[1] + 10*end_square[0] + end_square[1]

    

    def get_notation(self, start_square, end_square, board): 
        """ Given start square and end square of a move, it gives proper chess notation"""
        notation = board[self.start_square[0]][self.start_square[1]] + self.dict_col[end_square[1]] + self.dict_row[end_square[0]]
        if ("bp" in notation) or ("cp" in notation):
            notation = self.dict_col[end_square[1]] + self.dict_row[end_square[0]]
        if board[self.end_square[0]][self.end_square[1]] != "--":
            notation = board[self.start_square[0]][self.start_square[1]] + "x" + self.dict_col[end_square[1]] + self.dict_row[end_square[0]]
            if ("bp" in notation) or ("cp" in notation):
                notation = self.dict_col[self.start_square[1]] + "x" + self.dict_col[end_square[1]] + self.dict_row[end_square[0]]
        return notation

    @staticmethod
    def clear_color(notation):
        chars_to_delete = ["c", "b"]
        for char in chars_to_delete:
            notation = notation.replace(char, "")
        return notation

    def __eq__(self, other):
        if isinstance(other, move):
            return self.move_id == other.move_id