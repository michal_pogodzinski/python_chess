import pygame
import ChessBoard 

WIDTH =  HEIGHT = 600
DIMENSION = 8
SQ_SIZE = HEIGHT // DIMENSION
MAX_FPS = 60
IMAGES = {}

def load_images():
    pieces = ["cW", "cS", "cG", "cH", "cK", "cG", "cS", "cW", "bW", "bS", "bG", "bH", "bK", "bG", "bS", "bW", "bp", "cp"]
    for piece in pieces:
        IMAGES[piece] = pygame.transform.scale(pygame.image.load("Pieces/{}.png".format(piece)), (SQ_SIZE, SQ_SIZE))


def ChessMain():
    pygame.init()
    screen = pygame.display.set_mode((WIDTH,HEIGHT))
    clock = pygame.time.Clock()
    screen.fill(pygame.Color("white"))
    state = ChessBoard.ChessState()
    load_images()
    running = True
    clicks = []
    sqaure_selected = ()
    valid_moves = state.get_all_valid_moves()
    draw_state(screen, state, valid_moves, sqaure_selected)
    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
            elif e.type == pygame.MOUSEBUTTONDOWN:
                x,y = pygame.mouse.get_pos() #returns x,y
                col = x//SQ_SIZE
                row = y//SQ_SIZE
                if sqaure_selected == (row, col):   #resets clicks if the same square is clicked
                    sqaure_selected = ()
                    clicks = []
                else:                               #append clicks
                    sqaure_selected = (row, col)
                    clicks.append(sqaure_selected)
                if len(clicks) == 2:                #make a move
                    move = ChessBoard.move(clicks[0], clicks[1], state.board)
                    if move in valid_moves:
                        state.make_move(move)
                        state.check_if_move_was_pass()
                        state.check_if_move_was_castle() 
                        valid_moves = state.get_all_valid_moves()
                    sqaure_selected = ()
                    clicks = []
            # if e.type == pygame.KEYDOWN:
            #     if e.key == pygame.K_r:
            #         state = ChessBoard.ChessState()
            #         clicks = []
            #         sqaure_selected = ()

        # THE ENDGAME
        if state.checkmate_or_stalemate(valid_moves):
            if state.Whiteturn:
                print("Black wins via Checkmate!")
            else:
                print("White wins via Checkmate!")
            running = False
        elif state.checkmate_or_stalemate(valid_moves) == False:
            print("Stalemate!")
            running = False

        draw_state(screen,state, valid_moves, sqaure_selected)
        clock.tick(MAX_FPS)
        pygame.display.flip()


def highlight_square(screen,state,valid_moves,sqaure_selected):
    """
    Highligts picked piece and its possible squares to move
    """
    if sqaure_selected:
        
        x,y = sqaure_selected
        if state.board[x][y][0] == ("b" if state.Whiteturn else "c"):
            surface = pygame.Surface((SQ_SIZE, SQ_SIZE))
            surface.set_alpha(100)
            surface.fill(pygame.Color("blue"))
            screen.blit(surface,(y*SQ_SIZE,x*SQ_SIZE))
            surface.fill(pygame.Color("grey"))
            for move in valid_moves:
                print(move.end_square)
                if move.start_square == (x,y):
                    screen.blit(surface,(SQ_SIZE*move.end_square[1], SQ_SIZE*move.end_square[0]))
                    print(move.end_square)


def draw_state(screen, state, valid_moves, sqaure_selected):
    """ Draws current game state """
    draw_squares(screen)
    highlight_square(screen, state, valid_moves, sqaure_selected)
    draw_pieces(screen, state.board)

def draw_squares(screen):
    colors = [pygame.Color("white"), pygame.Color("dark green")]
    for row in range(DIMENSION):
        for column in range(DIMENSION):
            color = colors[((row+column)%2)]
            pygame.draw.rect(screen, color, pygame.Rect(column*SQ_SIZE, row*SQ_SIZE, SQ_SIZE, SQ_SIZE))

def draw_pieces(screen, state):
    for row in range(DIMENSION):
        for column in range(DIMENSION):
            piece = state[row][column]
            if piece != "--":
                screen.blit(IMAGES[piece], pygame.Rect(column*SQ_SIZE, row*SQ_SIZE, SQ_SIZE, SQ_SIZE))

def check_if_click_is_valid(state, clicks):
    pass

if __name__ == "__main__":
    ChessMain()

